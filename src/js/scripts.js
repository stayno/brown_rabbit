//Caroulsel Code
var $item = $('.carousel-item');
var $wHeight = '360px';

$item.height($wHeight); 
//Show the whole picture
$item.addClass('full-screen');

//Get total number of slides
var $numberofSlides = $('.carousel-item').length;
//Random slide
var $currentSlide = Math.floor((Math.random() * $numberofSlides));

$('.carousel-indicators li').each(function(){
	var $slideValue = $(this).attr('data-slide-to');
	if($currentSlide == $slideValue) {
		$(this).addClass('active');
		$item.eq($slideValue).addClass('active');
	}else{
		$(this).removeClass('active');
		$item.eq($slideValue).removeClass('active');
	}
});

$('.carousel img').each(function(){
	var $src = $(this).attr('src');
	$(this).parent().css({
		'background-image' : 'url(' + $src + ')'
	});
	$(this).remove();
});

$('.carousel').carousel({
	interval: 6000,
	pause: "false"
});

//Read more button Code
function readMoreFunction(el){
	var elToShowHide = $(el).closest('div').find('p .readMore');
	var dotsToShowHide = $(el).closest('div').find('p .readMoreDots');
	var txt = $(elToShowHide).is(':visible') ? 'Read more' : 'Read less';
    $(el).text(txt);
	$(elToShowHide).toggle();
	$(dotsToShowHide).toggle();
};

//Pagination code

//get total pages number, considering we show 3 per page and we round it upwards to the nearest nr
var totalPages = Math.ceil($('.blog-post').length/3);

//Build Pagination
var startPagiHtml = "<li id='liPrev' class='page-item disabled'><a id='arrowPrev' class='page-link' href='#'>Previous</a></li>";
var pagesPagiHtml = startPagiHtml;
var i;
for( i=0; i < totalPages; i++ ){
	if(i==0){
		pagesPagiHtml = pagesPagiHtml+"<li class='page-item active'><a class='page-link' href='#'>"+(i+1)+"</a></li>"
	}else{
		pagesPagiHtml = pagesPagiHtml+"<li class='page-item'><a class='page-link' href='#'>"+(i+1)+"</a></li>"
	}
}
var endPagiHtml = pagesPagiHtml+"<li id='liNext' class='page-item'><a id='arrowNext' class='page-link' href='#'>Next</a></li>"

$("#blogPagination").append(endPagiHtml);

//Pagination click code
$('ul.pagination li a').on('click',function(e){
	e.preventDefault();
	$('.blog-post').hide();
	var pageNr = $(this).text();
	if($.isNumeric(pageNr)){
		showPage(pageNr, this);
	}else{
		if($(this).attr("id")=="arrowNext"){
			nextPage();
		}else{
			prevPage();
		}
	}
	
});

//Show page
function showPage(nr, el){
	$('.pagination').find(".active").removeClass("active");
	var pageToShow = ".pageSet"+nr;
	$(pageToShow).show();
	$(el).closest('li').addClass('active');
	
	if(nr==totalPages){
		$('#liNext').addClass('disabled');
	}else{
		if($('#liNext').hasClass('disabled')){
			$('#liNext').removeClass('disabled');
		}
	}
	
	if(nr==1){
		$('#liPrev').addClass('disabled');
	}else{
		if($('#liPrev').hasClass('disabled')){
			$('#liPrev').removeClass('disabled');
		}
	}
};

//Using the next arrow
function nextPage(){
	var currentPage = $('.pagination').find(".active");
	$(currentPage).removeClass("active");
	var nextPageNr = parseInt($(currentPage).text())+1;
	showPage(nextPageNr, $(currentPage).next());
};

//Using the previous arrow
function prevPage(){
	var currentPage = $('.pagination').find(".active");
	$(currentPage).removeClass("active");
	var prevPageNr = parseInt($(currentPage).text())-1;
	showPage(prevPageNr, $(currentPage).prev());
};



